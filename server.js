let express = require('express'),
bodyParser = require('body-parser'),
hateoasLinker = require('express-hateoas-links')
app = express()

// ---- Moteur de template ---- //
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs')

// Application de body parse (Parse le retour des requètes en JSON)
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))
app.set('json spaces', 2)

// Application de hateoasLinker (Pour retourner les liens avec identifiant)
app.use(hateoasLinker);

// ---- Routes ---- //
app.use(require('./routes/index'))
app.use(require('./routes/personne'))
app.use(require('./routes/404'))

// ---- Serveur ---- //
app.listen(8080, () => {
  console.log('Serveur expressjs-elasticsearch ouvert sur le port 8080 !')
})
