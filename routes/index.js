let express = require('express'),
    router = express.Router(),
    servicePersonne = require('../services/personne')

// Page Accueil
router.get('/', (request, response) => {

  // Appel de la fonction de recherche de toutes les personnes
  servicePersonne.rechercherPersonnes((result) => {

    // Renseigne la liste des personnes
    let personnes = []

    // Parcours le résultat
    result.forEach((personne) => {
      // Ajoute les données le la personnes dans la liste
      personnes.push(personne._source)
    })

    // Retourne la page d'accueil avec la liste des personnes
    response.render('index.html', {personnes: personnes})
  })

})

module.exports = router
