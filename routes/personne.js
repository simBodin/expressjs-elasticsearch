let express = require('express'),
    router = express.Router(),
    servicePersonne = require('../services/personne')

// Ajouter une personne avec les données passés en paramètre
router.post('/personne/ajouter', (request, response) => {

  // Récupération des données en paramètre
  let personne = JSON.stringify(request.body)
  console.log("personne/ajouter : " + personne)

  // Appel de la fonction d'ajout d'une personne
  servicePersonne.ajouterPersonne(personne, (result) => {
    // Retourne le résultat
    response.json(result,
      [{
          rel: "self",
          method: "GET",
          href: 'http://localhost:8080/personne/' + result._id
      }]
    )
  })

})

// Modifier une personne avec les données passés en paramètre
router.post('/personne/modifier/:id', (request, response) => {

  // Récupération de l'identifiant donnée en paramètre
  let id = request.params.id
  console.log("personne/modifier/" +  id)

  // Récupération des données en paramètre
  let personne = JSON.stringify(request.body)
  console.log(personne)

  // Appel de la fonction d'ajout d'une personne
  servicePersonne.modifierPersonne(id, personne, (result) => {
    // Retourne le résultat
    response.json(result,
      [{
          rel: "self",
          method: "GET",
          href: 'http://localhost:8080/personne/' + result._id
      }]
    )
  })

})

// Supprime une personne en fonction de l'identifiant passé en paramètre
router.get('/personne/supprimer/:id', (request, response) => {

  // Récupération de l'identifiant donnée en paramètre
  let id = request.params.id
  console.log("personne/supprimer/" +  id)

  // Appel de la fonction de suppression de personne
  servicePersonne.supprimerPersonne(id, (result) => {
    // Retourne le résultat
    response.json(result)
  })

})

// Retourne une personne en fonction de l'identifiant passé en paramètre
router.get('/personne/rechercher/:id', (request, response) => {

  // Récupération de l'identifiant donnée en paramètre
  let id = request.params.id
  console.log("personne/" +  id)

  // Appel de la fonction de recherche de personne
  servicePersonne.rechercherPersonne(id, (result) => {
    // Retourne le résultat
    response.json(result,
      [{
          rel: "self",
          method: "GET",
          href: 'http://localhost:8080/personne/' + result._id
      }]
    )
  })

})

// Retourne une liste de personne en fonction du mot clé passé en paramètre
router.get('/personnes/rechercher/:cle', (request, response) => {

  // Récupération de la clé donnée en parmètre
  let cle = request.params.cle
  console.log("personnes/" +  cle)

  // Appel de la fonction de recherche par clé
  servicePersonne.rechercherPersonnes(cle, (result) => {

    // Parcours les personnes
    result.forEach((personne) => {

      // Ajoute les liens (HATEOAS)
      personne["links"] =
        [{
          rel: "self",
          method: "GET",
          href: 'http://localhost:8080/personne/' + personne._id
        }]
    })
    // Retourne le résultat
    response.send(result)
  })

})

// Retourne la liste de toutes les personnes
router.get('/personnes/rechercher', (request, response) => {

  // Appel de la fonction de recherche de toutes les personnes
  servicePersonne.rechercherPersonnes((result) => {

    // Parcours les personnes
    result.forEach((personne) => {

      // Ajoute les liens (HATEOAS)
      personne["links"] =
        [{
          rel: "self",
          method: "GET",
          href: 'http://localhost:8080/personne/' + personne._id
        }]
    })
    // Retourne le résultat
    response.send(result)
  })

})

module.exports = router
