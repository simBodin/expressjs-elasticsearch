let express = require('express'),
    router = express.Router()

// Page Erreur 404
router.get('*', (request, response) => {
  response.render('404.html')
})

module.exports = router
