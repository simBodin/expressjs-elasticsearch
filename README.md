# Require ElasticSearch en base de données (Port 9200) #


# Installation des modules #
npm install

# Execution du programme #
npm start

# Plusieurs Actions disponible (REST) :  #

# Creation Personne (POST)
- http://localhost:8080/personne/ajouter
- Param exemple :
{
   "nom": "Jackie",
   "prenom": "Michel",
   "commentaire": "Aime les bonnes choses"
}

# Modification Personne  (POST)
- http://localhost:8080/personne/modifier/id (id =  identifiant d'une personne)
- Param exemple :
{
   "nom": "Jackie",
   "prenom": "Michel",
   "commentaire": "Aime les bonnes choses et plus encore"
}

# Suppression Personne (GET)
- http://localhost:8080/personne/supprimer/id (id =  identifiant d'une personne)

# Recherche Personne Par identifiant (GET)
- http://localhost:8080/personne/rechercher/id (id =  identifiant d'une personne)

# Recherche Personnes Par Mot cle (GET)
- http://localhost:8080/personnes/rechercher/cle (key =  mot cle pour une recherche global)

# Recherche toutes les Personnes (GET)
- http://localhost:8080/personnes/rechercher

# Page Accueil, liste les personnes présentes dans la BDD
- http://localhost:8080

# Page 404, afficher si URL non spécifié dans l'application