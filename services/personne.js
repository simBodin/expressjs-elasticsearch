let elasticsearch = require('elasticsearch')

// Déclaration de la BDD elasticsearch
let elasticClient = new elasticsearch.Client({
    host: 'localhost:9200',
    log: 'trace'
})

// Ajoute une personne dans la BDD
// personne =  flux JSON de la personne
exports.ajouterPersonne = (personne, retour) => {
  elasticClient.index({
    index: 'meetup',
    type: 'personne',
    body: personne
  }).then((reponse) => {
    retour(reponse)
  }, (error) => {
    retour(error)
  })
}

// Modification d'une personne dans la BDD
// id = identifiant de la personne
// personne =  flux JSON de la personne
exports.modifierPersonne = (id, personne, retour) => {
  elasticClient.index({
    index: 'meetup',
    type: 'personne',
    id: id,
    body: personne
  }).then((reponse) => {
    retour(reponse)
  }, (error) => {
    retour(error)
  })
}

// Suppression d'une personne dans la BDD
// id = identifiant de la personne
exports.supprimerPersonne = (id, retour) => {
  elasticClient.delete({
    index: 'meetup',
    type: 'personne',
    id: id
  }).then((reponse) => {
    retour(reponse)
  }, (error) => {
    retour(error)
  })
}

// Recherche une personne dans la BDD
// id = identifiant de la personne
exports.rechercherPersonne = (id, retour) => {
  elasticClient.get({
    index: 'meetup',
    type: 'personne',
    id: id
  }).then((reponse) => {
    retour(reponse)
  }, (error) => {
    retour(error)
  })
}

// Recherche des personnes dans la BDD
// cle = mot clé de recherche des personnes
exports.rechercherPersonnes = (cle, retour) => {
  elasticClient.search({
    index: 'meetup',
    type: 'personne',
    body: {
      query: {
          query_string: {
              query: cle
          }
      }
    }
  }).then((reponse) => {
    retour(reponse.hits.hits)
  }, (error) => {
    retour(error)
  })
}

// Recherche toutes les personnes dans la BDD
exports.rechercherPersonnes = (retour) => {
  elasticClient.search({
    index: 'meetup',
    type: 'personne',
    body: {
      query: {
        match_all: {}
      }
    }
  }).then((reponse) => {
    retour(reponse.hits.hits)
  }, (error) => {
    retour(error)
  })
}
